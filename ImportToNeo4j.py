from neo4j import GraphDatabase
import re
from bson.json_util import loads, dumps


class TweeterGraph:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    # --------------------------------------------- create user node ---------------------------------------------------
    def create_user(self, data):
        with self.driver.session() as session:
            message = session.write_transaction(self._create_user_and_return_message, data)
            # print(message)

    @staticmethod
    def _create_user_and_return_message(tx, data):
        result = tx.run("MERGE (u:User {name: $name,followers: $followers}) "
                        "RETURN u.name + ' is created and has ' + u.followers +' followers.'", name=data[0],
                        followers=data[1])
        return result.single()[0]

    # --------------------------------------------- create tweet node --------------------------------------------------

    def create_tweet(self, data):
        with self.driver.session() as session:
            message = session.write_transaction(self._create_tweet_and_return_message, data)
            # print(message)

    @staticmethod
    def _create_tweet_and_return_message(tx, data):
        result = tx.run("CREATE (t:Tweet {text: $text,id: $id}) "
                        "RETURN t.id + ' tweet is created.'", text=data[0], id=data[1])
        return result.single()[0]

    # -------------------------------------------- create hashtag node -------------------------------------------------

    def create_hashtag(self, data):
        with self.driver.session() as session:
            message = session.write_transaction(self._create_hashtag_and_return_message, data)
            # print(message)

    @staticmethod
    def _create_hashtag_and_return_message(tx, data):
        result = tx.run("MERGE (h:Hashtag {name: $name}) "
                        "RETURN h.name + ' is created.'", name=data[0])
        return result.single()[0]

    # --------------------------------------- create user & tweet relation ---------------------------------------------
    def create_user_tweet_rel(self, data):
        with self.driver.session() as session:
            message = session.write_transaction(self._create_user_tweet_and_return_message, data)
            # print(message)

    @staticmethod
    def _create_user_tweet_and_return_message(tx, data):
        result = tx.run(
            "MATCH (u:User), (t:Tweet) WHERE u.name=$name and t.id=$id MERGE (u)-[a:TWEETED]->(t)"
            "RETURN u.name + ' tweeted the tweet with id = ' + t.id", name=data[0], id=data[1])
        return result.single()[0]

    # --------------------------------------- create hashtag & hashtag relation ---------------------------------------------
    def create_hashtag_hashtag_rel(self, data):
        with self.driver.session() as session:
            message = session.write_transaction(self._create_hashtag_hashtag_and_return_message, data)
            # print(message)

    @staticmethod
    def _create_hashtag_hashtag_and_return_message(tx, data):
        result = tx.run(
            "MATCH (x:Hashtag), (h:Hashtag) WHERE x.name = $hashtag1 and h.name=$hashtag2 CREATE (x)-[b:CAME_WITH]->(h)"
            "RETURN '#'+x.name + ' came with #' + h.name", hashtag1=data[0], hashtag2=data[1])
        return result.single()[0]

    # --------------------------------------- create user & hashtag relation ---------------------------------------------
    def create_user_hashtag_rel(self, data):
        with self.driver.session() as session:
            message = session.write_transaction(self._create_user_hashtag_and_return_message, data)
            # print(message)

    @staticmethod
    def _create_user_hashtag_and_return_message(tx, data):
        result = tx.run(
            "MATCH (u:User), (h:Hashtag) WHERE u.name = $name and h.name=$hashtag MERGE (u)-[c:USED_HASHTAG]->(h)"
            "RETURN u.name + ' used hashtag #' + h.name", name=data[0], hashtag=data[1])
        return result.single()[0]
    # ------------------------------------------ end of the class ------------------------------------------------------


def cleanse_tweet_and_return_useful_information(tweet):
    json = dumps(tweet)
    tweet_json = loads(json)
    tweet_json = re.sub(r'ObjectId[(a-z0-9)"]*', '0', tweet_json)
    tweet_json = re.sub(r'NumberLong[a-z()0-9"]*', '0', tweet_json)
    tweet_dict = loads(tweet_json)
    useful_information = {}
    useful_information['text'] = tweet_dict['Text']
    useful_information['tweet_id'] = tweet_dict['TweetId']
    useful_information['user_name'], useful_information['user_followers'] = tweet_dict['TwitterUserEntityModel'][
                                                                                'ScreenName'], \
                                                                            tweet_dict['TwitterUserEntityModel'][
                                                                                'FollowersCount']
    useful_information['hashtags'] = []
    for hashtag in tweet_dict['Entities']['Hashtag']:
        useful_information['hashtags'].append(hashtag['Text'])
    return useful_information


if __name__ == "__main__":
    heyNeo = TweeterGraph("bolt://localhost:7687", "neo4j", "1234")

    for tweet in open('Tweets.txt', encoding="utf8"):
        tweet = cleanse_tweet_and_return_useful_information(tweet)
        heyNeo.create_user([tweet['user_name'], tweet['user_followers']])
        heyNeo.create_tweet([tweet['text'], tweet['tweet_id']])
        heyNeo.create_user_tweet_rel([tweet['user_name'], tweet['tweet_id']])
        # heyNeo.create_hashtag_hashtag_rel([tweet['tweet_id'], hashtag])
        hashtags, hashtags_len = tweet['hashtags'], len(tweet['hashtags'])
        for h in hashtags:
            heyNeo.create_hashtag([h])
            heyNeo.create_user_hashtag_rel([tweet['user_name'], h])
        for i in range(0, hashtags_len):
            if i < (hashtags_len - 1):
                for j in range(i + 1, hashtags_len):
                    heyNeo.create_hashtag_hashtag_rel([hashtags[i], hashtags[j]])
        print(tweet['user_name'], ' is inserted.')

    heyNeo.close()
