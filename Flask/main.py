from flask import Flask
from flask import request
from markupsafe import escape
from neo4j import GraphDatabase

app = Flask(__name__)


def get_effective_users_and_repetitive_hashtags(hashtag):
    driver = GraphDatabase.driver("bolt://localhost:7687", auth=('neo4j', '1234'))
    with driver.session() as session:
        effective_users_query = session.run(
            "MATCH (u)-[c:USED_HASHTAG]-(h) WHERE h.name=$hashtag RETURN u.name as name,u.followers as followers ORDER BY u.followers DESC LIMIT 10",
            hashtag=hashtag)
        repetitive_hashtags_query = session.run(
            "MATCH (t)-[c:CAME_WITH]-(h) WHERE t.name=$hashtag RETURN h.name as name,count(c) ORDER BY count(c) DESC LIMIT 10",
            hashtag=hashtag)
        effective_users = []
        repetitive_hashtags = []
        for user in effective_users_query:
            effective_users.append(user['name'])
        for hashtag in repetitive_hashtags_query:
            repetitive_hashtags.append(hashtag['name'])

        driver.close()
        return effective_users, repetitive_hashtags


@app.route('/')
def get_hashtag():
    hashtag = escape(request.args.get("hashtag"))
    effective_users, repetitive_hashtags = get_effective_users_and_repetitive_hashtags(hashtag)
    data = {"repetitive_hashtags": repetitive_hashtags, "effective_users": effective_users}
    return data
